<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage akto
 * @since akto
 */

get_header(); 
//If we are in the courses arhive page
if(is_post_type_archive('courses')){
	//Get all courses
	$query = new WP_Query(array(
		'numberposts'	=> -1,
		'post_type'		=> 'courses'
	));
}else{
	//Get all posts
	$query = new WP_Query(array(
		'numberposts'	=> -1,
		'post_type'		=> 'posts'
	));	
}
$posts = $query->posts;
?>
<main id="general-list" class="background-grid">
<section class="front-container">
	<div class="uk-margin-remove uk-padding-remove" uk-grid>
		<div class="uk-width-auto uk-margin-remove uk-padding-remove">
		</div>
		<h1><?php get_the_title();?></h1>
		<div class="uk-width-expand uk-margin-remove uk-padding-remove">
			<div class="bg-color uk-background-red-deep uk-animation-slide-right"></div>
			<div class="bg-wrapper">
				<div class="uk-animation-slide-right" style="background-image: url(<?=$image;?>);background-position:center;background-size:cover; ">
				</div>
			</div>
		</div>
	</div>
	<?php get_template_part('template-parts/breadcrumb'); ?>	
</section>
<section class="listing">
</section>
</main>
<?php get_footer(); ?>
