<?php
/*
	Template Name: Contact Template
*/
//LOAD ACF FIELDS
$image = wp_get_attachment_image_url( get_post_thumbnail_id(get_the_ID()),'cover');
get_header(); ?>
<main id="contact" class="background-grid">
	<section class="front-container">
		<?php get_template_part('template-parts/breadcrumb'); ?>	
		<div class="uk-margin-remove uk-padding-remove" uk-grid>
			<div class="uk-width-auto uk-margin-remove uk-padding-remove"></div>
			<h1><?php the_title();?></h1>
			<div class="uk-width-expand uk-margin-remove uk-padding-remove">
				<div class="bg-color uk-animation-slide-right"></div>
				<div class="bg-wrapper">
					<div class="uk-animation-slide-right" style="background-image: url(<?=$image;?>);background-position:center;background-size:cover;image-rendering: -webkit-optimize-contrast; ">
					</div>
				</div>
			</div>
		</div>	
	</section>
	<section id="form-box" uk-scrollspy="cls: uk-animation-slide-left; repeat: false;delay:600;">
		<?php include( locate_template( 'template-parts/forms/contact-form.php', false, false ) ); ?>
	</section>
</main>
<?php get_footer(); ?>
