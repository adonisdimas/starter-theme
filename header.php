<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage starter
 * @since 1.0
 * @version 1.0
 */
//Load theme settings custom fields
global $home_url,$error_messages;
$error_messages = get_field('error_messages','option');
$general_labels = get_field('general_labels','option');
$form_submit = get_field('form_submit','option');
$home_url = apply_filters( 'wpml_home_url', get_option( 'home' ) );
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg moving-shapes">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<header>
	<div class="uk-container uk-container-expand uk-flex uk-flex-between uk-flex-top">
		<div class="logo">
			<a title='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>' href="<?php echo $home_url; ?>">
				<span uk-icon="logo"></span>
			</a>
		</div>
	</div>
</header>

