<?php
/**
 * starter functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage starter
 * @since 1.0
 */

require get_template_directory(). '/inc/enqueue-styles-scripts.php';
require get_template_directory(). '/inc/customizer.php';
require get_template_directory(). '/inc/custom-functions.php';
require get_template_directory(). '/inc/custom-db.php';
require get_template_directory(). '/inc/custom-admin.php';
require get_template_directory(). '/inc/custom-fields.php';
require get_template_directory(). '/inc/custom-post-types.php';
require get_template_directory(). '/inc/custom-widget-areas.php';
require get_template_directory(). '/inc/custom-widgets.php';

// Adding recaptcha to forms
function add_custom_recaptcha_forms( $forms ) {
    $forms['starter_contact_form'] = array( "form_name" => "contact-form" );
    $forms['starter_general_form'] = array( "form_name" => "general-form" );
    return $forms;
}
add_filter( 'gglcptch_add_custom_form', 'add_custom_recaptcha_forms' );


/**
 * Load CSS and JS the right way
 */
function myprefix_load_css_and_js() {
    wp_enqueue_style( 'style', get_stylesheet_uri() );
}
add_action( 'wp_enqueue_scripts', 'myprefix_load_css_and_js' );

// REMOVE EMOJI ICONS
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');
remove_action('wp_head', 'wp_resource_hints', 2);

function starter_theme_setup() {
    add_theme_support( 'post-thumbnails');
    add_image_size( 'cover', 1300, 650 ,true);
}
add_action( 'after_setup_theme', 'starter_theme_setup' );

/*  Thumbnail upscale
/* ------------------------------------ */ 
function starter_thumbnail_upscale( $default, $orig_w, $orig_h, $new_w, $new_h, $crop ){
    if ( !$crop ) return null; // let the wordpress default function handle this
 
    $aspect_ratio = $orig_w / $orig_h;
    $size_ratio = max($new_w / $orig_w, $new_h / $orig_h);
 
    $crop_w = round($new_w / $size_ratio);
    $crop_h = round($new_h / $size_ratio);
 
    $s_x = floor( ($orig_w - $crop_w) / 2 );
    $s_y = floor( ($orig_h - $crop_h) / 2 );
 
    return array( 0, 0, (int) $s_x, (int) $s_y, (int) $new_w, (int) $new_h, (int) $crop_w, (int) $crop_h );
}
add_filter( 'image_resize_dimensions', 'starter_thumbnail_upscale', 10, 6 );

add_filter( 'image_size_names_choose', 'my_custom_sizes' );
function my_custom_sizes( $sizes ) {
    return array_merge( $sizes, array(
        'starter-featured-image' => __( 'Featured Image' ),
        'starter-featured-image-small' => __( 'Featured Image Small' )
    ) );
}

//Add svg support
function cc_mime_types( $mimes ){
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}
add_filter( 'upload_mimes', 'cc_mime_types' );

/**
 * Register nav menu.
 *
 * @link https://codex.wordpress.org/Function_Reference/register_nav_menus
 */
function starter_register_nav_menu()
{
    register_nav_menu('primary', 'Box Menu 1');
    register_nav_menu('secondary', 'Box Menu 2');
    register_nav_menu('mobile-menu', 'Mobile Menu');
}
add_action('after_setup_theme', 'starter_register_nav_menu');

// unregister all default wordpress widgets
 function remove_default_widgets() {
    unregister_widget('WP_Widget_Pages');
    unregister_widget('WP_Widget_Calendar');
    unregister_widget('WP_Widget_Archives');
    unregister_widget('WP_Widget_Links');
    unregister_widget('WP_Widget_Meta');
    unregister_widget('WP_Widget_Search');
    unregister_widget('WP_Widget_Categories');
    unregister_widget('WP_Widget_Recent_Posts');
    unregister_widget('WP_Widget_Recent_Comments');
    unregister_widget('WP_Widget_RSS');
    unregister_widget('WP_Widget_Tag_Cloud');
    unregister_widget('WP_Widget_Media_Video');
    unregister_widget('WP_Widget_Media_Audio');
    unregister_widget('WP_Widget_Media_Image');
 }
 add_action('widgets_init', 'remove_default_widgets', 11);