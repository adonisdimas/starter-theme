<?php
/*
	Template Name: General Form Template
*/
get_header(); 
//LOAD  FIELDS
$image = wp_get_attachment_image_url( get_post_thumbnail_id(get_the_ID()),'cover');
$general_form = get_field('general_form','option');
//get the options of tge 
$general_form_selected_option = get_field('select_option_list');
$general_messages = get_field('general_form_messages','option');
?>
<main id="general-form" class="background-grid">
	<section class="front-container">
		<div class="uk-margin-remove uk-padding-remove" uk-grid>
			<div class="uk-width-auto uk-margin-remove uk-padding-remove"></div>
			<h1><?php the_title();?></h1>
			<div class="uk-width-expand uk-margin-remove uk-padding-remove">
				<div class="bg-color uk-background-secondary uk-animation-slide-right"></div>
				<div class="bg-wrapper">
					<div class="uk-animation-slide-right" style="background-image: url(<?=$image;?>);background-position:center;background-size:cover; ">
					</div>
				</div>
			</div>
		</div>
		<?php get_template_part('template-parts/breadcrumb'); ?>
	</section>
	<section class="content">
		<div class="uk-margin-remove uk-padding-remove" uk-grid>
			<div class="uk-width-auto uk-margin-remove uk-padding-remove">
			</div>
			<div class="uk-width-expand uk-margin-remove uk-padding-remove">
				<div class="text-wrapper">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</section>
	<section id="form-box" uk-scrollspy="cls: uk-animation-slide-left; repeat: false;delay:600;">
		<?php include( locate_template( 'template-parts/forms/general-form.php', false, false ) ); ?>
	</section>
</main>
<?php get_footer(); ?>
