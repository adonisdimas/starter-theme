var	moment = require('moment'),
		gulp = require('gulp'),
		plumber = require('gulp-plumber'),
		rename = require('gulp-rename'),
		autoprefixer = require('gulp-autoprefixer'),
		minifycss = require('gulp-minify-css'),
		sass = require('gulp-sass'),		
		header = require('gulp-header'),
		pkg = require('./package.json'),
		include = require('gulp-include'),
		sourcemaps = require('gulp-sourcemaps'),
		uglify = require('gulp-uglify'),
		paths = {
			'dev': { 'style' : 'dev/sass/', 'js' : 'dev/js/' },
			'build': { 'style' : 'build/css/', 'js' : 'build/js/' },
			'bower': 'dev/third-party/'
		};
gulp.task('styles', function(){
	gulp.src([paths.dev.style+'main.sass'])
	// .pipe(sourcemaps.init())
	.pipe(plumber({ errorHandler: function (error) { console.log(error.message); this.emit('end'); }}))
	.pipe(sass({outputStyle: 'compressed'}))
	.pipe(autoprefixer('last 99 versions', 'ie 10'))
	.pipe(rename({suffix: '.min'}))
	.pipe(minifycss())
	// .pipe(sourcemaps.write('.'))
	.pipe(gulp.dest(paths.build.style))
});


gulp.task('js', function () {
	return gulp.src(paths.dev.js+'main.js')
	.pipe(plumber({ errorHandler: function (error) { console.log(error.message); this.emit('end'); } }))
	.pipe(include({
		extensions: "js",
		hardFail: true
	}))
	.pipe(sourcemaps.init())
	.pipe(header(banner, { pkg : pkg } ))
	.pipe(sourcemaps.write('.'))
	.pipe(gulp.dest(paths.build.js));
});	

gulp.task('default', function(){
  gulp.watch(paths.dev.style+"**/*.sass", ['styles']);
  gulp.watch(paths.dev.js+"*.js", ['js']);
});