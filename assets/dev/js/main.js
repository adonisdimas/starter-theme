/* Global settings */
var settings = {
	debug : false
};
/* Output to console if debug is on */
function echo(message) {
	if (settings.debug) {
		console.log(message);
	}
};
/* IE detection flag */
jQuery.browser = {};
(function() {
	jQuery.browser.msie = false;
	jQuery.browser.version = 0;
	if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
		jQuery.browser.msie = true;
		jQuery.browser.version = RegExp.$1;
	}
})();
/* Global help-vars */
var resizeTimeout;
(function($) {
	var app = {
		'init' : function(e) {
			echo('App init...');
			app.manageStickyHeader();
			app.manageNavigation();
			app.manageScroll();
			app.manageForms();
			app.manageSearch();
			$(window).on('resize', app.onResize);
		},
		'manageSearch' : function() {
			if( $('#menu-box .search').length ){ 
				$('#menu-box .search').on('click',function(e){
					$('#menu-box .search-box').toggleClass('active');
				});
				// USe this to close the search when somebody clicks outside the menu box
				$(document).on('click', function (e) {
				    if ($(e.target).closest("#menu-box").length === 0) {
				       $('#menu-box .search-box').removeClass('active');
				    }
				});				
			}
			if( $('#menu-box-modal .search').length ){ 
				$('#menu-box-modal .search').on('click',function(e){
					if ($(e.target).closest(".search-box").length === 0) {
						$('#menu-box-modal .search-box').toggleClass('active');
					}
				});
				// USe this to close the search when somebody clicks outside the menu box
				$(document).on('click', function (e) {
				    if ($(e.target).closest("#menu-box-modal .search").length === 0) {
				       $('#menu-box-modal .search-box').removeClass('active');
				    }
				});				
			}
			if( $('#menu-box input#site-search').length ){ 
				$( "#menu-box input#site-search" ).autocomplete({
					 source: function (request, response) {
					     // request.term is the term searched for.
					     // response is the callback function you must call to update the autocomplete's 
					     // suggestion list.
					     $.ajax({
					     	 type: "POST",
					         url: data['base_url']+'/wp-json/custom_search/results',
					         data: { s: request.term },
					         dataType: "json",
							 success: function(data) {
							 	var parsedData = JSON.parse(data)
								response(parsedData);
							 },
					         error: function () {
					             response([]);
					         }
					     })
					 },
					minLength: 3,
					open : function(event, ui){
						$(".ui-autocomplete:visible").css({top:"+=11",left:"-=21"});
					},
					select: function( event, ui ) {
						window.location = ui.item.link;
					},
				}).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
			        return $( "<li class='results_item'>" )
			        .append( '<a href="'+item.link+'">' + item.title +'</a>')
			        .appendTo( ul);
			    };
			}
		},
		'manageforms' : function() {
			//validate and submit apply online form
			if ( $("#apply" ).length ) {
				$('#apply a.submit').on('click',function(e){
					var validator =$('#apply').validate({});
					if(validator){
					    if(grecaptcha.getResponse() != ''){
					        $('#apply').submit();
					    }else{
					        //show the validator errors
					        validator.form();
					        var locale = data['locale'];
					        if(locale == 'el'){
					            validator.showErrors({
					                  'apply[captcha]': 'Το Recaptcha δεν έχει συμπληρωθεί'
					            });
					        }else{
					            validator.showErrors({
					                  'apply[captcha]': 'Verify that you are not a robot!'
					            });
					        }
					    }
					}else{
					    validator.form();
					}
				});
			}
			//validate and submit contaqct form
			if ( $("#contact-form" ).length ) {
				$('#contact-form a.submit').on('click',function(e){
				    var validator =$('#contact-form').validate({});
					if(validator){
						if(grecaptcha.getResponse() != ''){
							$('#contact-form').submit();
						}else{
							//show the validator errors
							validator.form();
							var locale = data['locale'];
							if(locale == 'el'){
								validator.showErrors({
								'contact[captcha]': 'Το Recaptcha δεν έχει συμπληρωθεί'
								});
							}else{
								validator.showErrors({
								'contact[captcha]': 'Verify that you are not a robot!'
								});
							}
					}
				    }else{
				        validator.form();
				    }
				});
			}
			//Validate and submit newsletter form
			if ( $("#newsletter-form" ).length ) {
				$('#subscribe').on('click',function(e){
					if($('#newsletter-form').validate()){
						$('#newsletter-form').submit();
					}

				});
			}
		},
		'manageStickyHeader' : function() {
			var header = $('header');
			$(document.body).on('touchmove', onScroll); // for mobile
			$(window).on('scroll', onScroll); 
			// callback
			function onScroll(){
				// var body = document.body;
				//// var docElem = document.documentElement;
				 //var scrollTop = window.pageYOffset || docElem.scrollTop || body.scrollTop;		    	
	    		if ($(document).scrollTop() > 100) {
		            header.addClass("sticky");
	    		}else{
	    			header.removeClass("sticky");
	    		}
		    }		
		},
		'manageScroll' : function() {
			//Click event to scroll to href link
			$('.smoothscroll').click(function(e){
				e.preventDefault();
				$('html, body').animate({
			        scrollTop: $( $.attr(this, 'href') ).offset().top-120
			    },1000);
				return false;
			});
		},
		'onResize' : function() {
			clearTimeout(resizeTimeout);
			resizeTimeout = setTimeout(function() {
				app.manageStickyHeader();
			}, 10);
		}
	};
	$(document).on('ready', app.init);
})(jQuery);
