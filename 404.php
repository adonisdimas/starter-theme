<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package WordPress
 * @subpackage starter
 * @since 1.0
 * @version 1.0
 */
//load custom fields
$error_page = get_field('error_page','option');
$general_labels = get_field('general_labels','option');
$image = wp_get_attachment_image_url( $error_page['image'],'starter-home-teaser1');
get_header(); ?>
<main id="error" class="background-grid">
	<div class="wrapper">
		<h1><?=$error_page['title'];?></h1>
		<p><?=$error_page['content'];?></p>
	</div>	
	<div class="image-wrapper" class="uk-animation-slide-right">
		<div class="bg-color"></div>
		<div class="bg-wrapper">
			<div class="uk-animation-slide-right" style="background-image: url(<?=$image?>);background-position:center;background-size:cover; ">
			</div>
		</div>
	</div>
	<?php if(isset($error_page['select_pages']) && $error_page['select_pages'] ): ?> 
		<div class="links-wrapper">
			<h2><?=$error_page['subtitle'];?></h2>
			<ul class="uk-nav">
				<?php foreach ($error_page['select_pages'] as $page):?>
					<li><a href="<?=get_permalink($page->ID)?>"><span uk-icon="icon: chevron-right; ratio: 1"></span><?=$page->post_title;?></a></li>
				<?php endforeach; ?>
			</ul>
		</div>
	<?php endif; ?>
</main>
<footer id="footer">
	<?php get_template_part('template-parts/footer-bottom'); ?>
</footer>
<?php wp_footer(); ?>
</body>
</html>
