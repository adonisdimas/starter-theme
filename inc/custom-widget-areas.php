<?php
/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function starter_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Homepage Widget Area', 'starter' ),
		'id'            => 'frontpage-area',
		'description'   => __( 'Add widgets here to apear after the main content of the frontpage .', 'starter' ),
	) );
	register_sidebar( array(
		'name'          => __( 'Page/Post Widget Area', 'starter' ),
		'id'            => 'post-main-area',
		'description'   => __( 'Add widgets here to apear after the main content of the post/page .', 'starter' ),
	) );
	register_sidebar( array(
		'name'          => __( 'Footer Area', 'starter' ),
		'id'            => 'footer-area',
		'description'   => __( 'Add widgets here to appear in footer area 1', 'starter' ),
		'before_widget' => '<div class="cell widget %2$s" id="footer-area1">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	) );
}
add_action( 'widgets_init', 'starter_widgets_init' );