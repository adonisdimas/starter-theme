<?php
/*
@package starter

	========================
		ADMIN PAGE
	========================
*/
function starter_add_admin_pages() {
	//Generate Admin Page
	add_submenu_page( 'starter_settings', 'starter Contact Submissions', 'Contact Submissions', 'manage_options', 'starter_contact_submissions', 'starter_contact_submissions_page' );
	add_submenu_page( 'starter_settings', 'starter Newsletter Submissions', 'Newsletter Submissions', 'manage_options', 'starter_newsletter_submissions', 'starter_newsletter_submissions_page' );
}
add_action( 'admin_menu', 'starter_add_admin_pages' );

//Template submenu functions
function starter_contact_submissions_page() {
	require_once( get_template_directory() . '/inc/templates/contact-submissions.php' );
}
function starter_newsletter_submissions_page() {
	require_once( get_template_directory() . '/inc/templates/newsletter-submissions.php' );
}