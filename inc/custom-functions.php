<?php
function starter_contact_mailer($recipients,$redirect) {
	// Get the posted data
	$contact_form = $_POST["contact"];
	$gdpr = $_POST["gdpr"];
	//Get the placeholders
	$contact_form_placeholders = get_field('contact_form','option');
	// write the email content
	$header = "MIME-Version: 1.0\n";
	$header .= "Content-Type: text/html; charset=utf-8\n";
	$header .= "From: info@starter.gr <info@starter.gr>";

	$message = "<strong>".$contact_form_placeholders["name"]." : </strong> ".$contact_form["firstname"]." <br>";
	$message .= "<strong>".$contact_form_placeholders["email"]." : </strong> ".$contact_form["email"]." <br>";
	$message .= "<strong>".$contact_form_placeholders["phone"]." : </strong> ".$contact_form["phone"]."<br>";
	$message .= "<strong>".$contact_form_placeholders["message"].": </strong>".$contact_form["message"]."<br><br>";

	if(isset($_POST["gdpr"]["sms"])){
		$message .= "<strong>GDPR SMS:</strong> ".$_POST["gdpr"]["sms"]."<br>";
	}
	if(isset($_POST["gdpr"]["tel"])){
		$message .= "<strong>GDPR TEL:</strong> ".$_POST["gdpr"]["tel"]."<br>";
	}
	if(isset($_POST["gdpr"]["email"])){
		$message .= "<strong>GDPR EMAIL:</strong> ".$_POST["gdpr"]["email"]."<br>";
	}
	if(isset($_POST["gdpr"]["post"])){
		$message .= "<strong>GDPR POST:</strong> ".$_POST["gdpr"]["post"]."<br>";
	}

	$subject = "starter NEW CONTACT FORM SUBMISSION";
	$subject = "=?utf-8?B?" . base64_encode($subject) . "?=";
	//Get recipients
	$to = $recipients;
	$page_id = $contact_form['page_id'];
	// send the email using wp_mail()
	if(wp_mail($to, $subject, $message, $header)) {
	    do_action( 'starter_contact_insert_hook', $_POST,1);
	}else{
		do_action( 'starter_contact_insert_hook', $_POST,0);
	}
	if(!empty($redirect)){
		wp_redirect( esc_url( $redirect ));
		exit;
	}else{
		wp_redirect( esc_url( get_permalink($page_id) ).'?sent=1');
		exit;
	}
}

function starter_newsletter() {
	if($_POST['newsletter']['honeycomb']==''){
		do_action( 'starter_newsletter_insert_hook', $_POST);
		$page_id = $_POST['newsletter']['page_id'];
		if($page_id){
			$newsletter_submitted = get_option( 'newsletter_submitted' );
			wp_redirect( esc_url( get_permalink($page_id) ).'?newsletter_submitted='.$newsletter_submitted);
			exit;
		}
	}
}

//Check all forms submissions here
function starter_form_submitions() {
	$recipients = get_field('recipients','option');
	$redirect = get_field('page_thank_you_redirect','option');
	if ( isset($_POST) && $_SERVER['REQUEST_METHOD'] === 'POST') {
		if(!empty($_POST['contact'])){
			//if recipients not empty send email and then insert else just insert
			if(!empty($recipients)){
				do_action( 'starter_contact_mailer_hook', $recipients,$redirect);
			}else{
				do_action( 'starter_contact_insert_hook', $_POST,0);
			}
			if(!empty($redirect)){
				wp_redirect( esc_url( $redirect ));
				exit;
			}else{
				$page_id = $_POST['contact']['page_id'];
				//Get the page of the contact from the parameter and then redirect there
				if($page_id){
					wp_redirect( esc_url( get_permalink($page_id) ).'?sent=1');
					exit;
				}				
			}
		}elseif(!empty($_POST['newsletter'])){
			do_action( 'starter_newsletter_hook');
		}
	}
}

add_action('starter_contact_mailer_hook', 'starter_contact_mailer',10,2);
add_action('starter_newsletter_hook', 'starter_newsletter');
add_action('init', 'starter_form_submitions');