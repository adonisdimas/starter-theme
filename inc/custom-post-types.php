<?php
/*
@package starter

	========================
		CUSTOM POST TYPES
	========================
*/
function starter_load_custom_post_types (){
	//Course post type
	$labels = array(
		'name' => 'Courses',
		'singular_name' => 'Course',
		'add_new' => 'Add Course',
		'all_items' => 'All Courses',
		'add_new_item' => 'Add Course',
		'edit_item' => 'Edit Course',
		'new_item' => 'New Course',
		'view_item' => 'View Course',
		'search_item' => 'Search Courses',
		'not_found' => 'No Courses found',
		'not_found_in_trash' => 'No Courses found in trash',
		'parent_item_colon' => 'Parent Item'
	);
	$args = array(
		'labels' => $labels,
		'public' => true,
		'has_archive' => false,
		'publicly_queryable' => true,
		'query_var' => true,
		'rewrite' => array('slug' => 'studies/course', 'hierarchical' => true ,'with_front' => true),
		'menu_icon' => 'dashicons-welcome-learn-more',
		'capability_type' => 'post',
		'hierarchical' => false,
		'supports' => array(
			'title',
			'editor',
			'thumbnail',
			'revisions',
		),
		'taxonomies' => array('course_level', 'course_faculty'),
		'menu_position' => 5,
		'exclude_from_search' => false
	);
	register_post_type('studies',$args);

	$labels = array(
		'name' => 'Levels',
		'singular_name' => 'Level',
		'add_new' => 'Add Level',
		'all_items' => 'All Levels',
		'add_new_item' => 'Add Level',
		'edit_item' => 'Edit Level',
		'new_item' => 'New Level',
		'view_item' => 'View Levels',
		'search_item' => 'Search Levels',
		'not_found' => 'No Levels found ',
		'not_found_in_trash' => 'No Levels found in trash',
		'parent_item_colon' => 'Parent Item'
	);
	register_taxonomy('course_level','studies', array(
		'hierarchical' => true,
		'labels' => $labels,
		'has_archive' => true,
		'sort' => true,
		'show_ui' => true,
		'show_admin_column' => true,
		'query_var' => true,
		'show_in_nav_menus' => true,
		'rewrite' => array('slug' => 'courses', 'hierarchical' => true ,'with_front' => true),
	));

	$labels = array(
		'name' => 'Faculties',
		'singular_name' => 'Faculty',
		'add_new' => 'Add Faculty',
		'all_items' => 'All Faculties',
		'add_new_item' => 'Add Faculty',
		'edit_item' => 'Edit Faculty',
		'new_item' => 'New Faculty',
		'view_item' => 'View Faculties',
		'search_item' => 'Search Faculties',
		'not_found' => 'No Course found ',
		'not_found_in_trash' => 'No Faculty found in trash',
		'parent_item_colon' => 'Parent Item'
	);
	register_taxonomy('course_faculty','studies', array(
		'hierarchical' => true,
		'labels' => $labels,
		'has_archive' => true,
		'sort' => true,
		'show_ui' => true,
		'show_admin_column' => true,
		'query_var' => true,
		'show_in_nav_menus' => true,
		'rewrite' => array('slug' => 'studies','with_front' => true),
	));
}
add_action('init','starter_load_custom_post_types');