<?php
function starter_migrations() {
	// Add tables to track form submissions
	//Newsletter
	global $wpdb;
	$table = $wpdb->prefix."starter_newsletter_submissions";
	$structure = "CREATE TABLE $table (
		id INT(9) NOT NULL AUTO_INCREMENT,
		name VARCHAR(200) NOT NULL,
		email VARCHAR(200) NOT NULL,
		UNIQUE KEY id (id)
	);";
	$wpdb->query($structure);
	//Contact form
	global $wpdb;
	$table = $wpdb->prefix."starter_contact_submissions";
	$structure = "CREATE TABLE $table (
		id INT(9) NOT NULL AUTO_INCREMENT,
		fname VARCHAR(200) NOT NULL,
		phone_num VARCHAR(200) NOT NULL,
		email VARCHAR(200) NOT NULL,
		message TEXT NOT NULL,
		gdpr_sms VARCHAR(10),
		gdpr_tel VARCHAR(10),
		gdpr_email VARCHAR(10),
		gdpr_post VARCHAR(10),
		send TINYINT(1),
		UNIQUE KEY id (id)
	);";
	$wpdb->query($structure);
}
add_action('after_switch_theme', 'starter_migrations');

function starter_contact_insert($data, $flag) {
	global $wpdb;
	$wpdb->query($wpdb->prepare("INSERT INTO ".$wpdb->prefix."starter_contact_submissions (fname, email, message, phone_num,gdpr_sms,gdpr_tel,gdpr_email,gdpr_post, send ) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s)",  $data["contact"]["firstname"],$data["contact"]["email"], $data["contact"]["message"] ,$data["contact"]["phone"],$data["gdpr"]["sms"],$data["gdpr"]["tel"],$data["gdpr"]["email"],$data["gdpr"]["post"],$flag));
}
add_action('starter_contact_insert_hook', 'starter_contact_insert',10,2);

function starter_newsletter_insert($data) {
	global $wpdb;
	$name = $data["newsletter"]["name"];
	$email = $data["newsletter"]["email"];
	if (is_email($email)) {
		$exists = $wpdb->get_row($wpdb->prepare("SELECT COUNT(`id`) as 'count' FROM ".$wpdb->prefix."starter_newsletter_submissions WHERE email = %s LIMIT 1", $email));
		if ((int) $exists->count === 0) {
			$wpdb->query($wpdb->prepare("INSERT INTO ".$wpdb->prefix."starter_newsletter_submissions (name, email) VALUES (%s, %s)", $name, $email));
			update_option( 'newsletter_submitted', '1' );
		}else{
			update_option( 'newsletter_submitted', '2' );
		}
	}else{
		update_option( 'newsletter_submitted', '0' );
	}
}
add_action('starter_newsletter_insert_hook', 'starter_newsletter_insert',10,2);

/**
 * Registers a setting.
 */
function starter_register_my_settings() {
	register_setting( 'my_options_group', 'newsletter_submitted', '-1' ); 
} 
add_action( 'after_switch_theme', 'starter_register_my_settings' );

//reset newsletter_submitted flag
update_option( 'newsletter_submitted', '-1' );