<div class="wrap">
	<h2>Contact Form Submissions</h2>
	<div id="poststuff">
		<div id="post-body" class="metabox-holder columns-1">
			<!-- main content -->
			<div id="post-body-content">
			<form method="post" action="?page=<?php echo esc_js(esc_html($_GET['page'])); ?>">
            <input name="submissions_remove" value="1" type="hidden" />
                        <?php 
                            if ($_SERVER['REQUEST_METHOD']=="POST" and $_POST['submissions_remove']) {
                                global $wpdb;
                                if ($_REQUEST['submit'] == 'Remove Selected') {
                                    if (isset($_GET['rem'])) $_POST['rem'][] = $_GET['rem'];
                                    $count = 0;
                                    if (is_array($_POST['rem'])) {
                                        foreach ($_POST['rem'] as $id) { 
                                            $wpdb->query("delete from ".$wpdb->prefix."akto_contact_submissions where id = '".esc_sql(($id))."' limit 1"); 
                                            $count++; 
                                        }
                                        $message = $count." submissions have been removed successfully.";
                                    }
                                }else if ($_REQUEST['submit'] == 'Clear all submissions'){

                                    $wpdb->query("TRUNCATE TABLE ".$wpdb->prefix."akto_contact_submissions"); 
                                    $message = "Submissions cleared.";
                                }
                            }
                        ?>
						<table cellspacing="0" class="wp-list-table widefat fixed subscribers">
                          <thead>
                            <tr>
                                <th style="" class="manage-column column-cb check-column" id="cb" scope="col"><input type="checkbox"></th>
                                <th style="" class="manage-column column-fname" id="fname" scope="col">Name:<span class="sorting-indicator"></span></th>
                                <th style="" class="manage-column column-email" id="email" scope="col"><span>Email Address:</span><span class="sorting-indicator"></span></th>
                                <th style="" class="manage-column column-send" id="send" scope="col"><span>Phone:</span><span class="sorting-indicator"></span></th>
                                <th style="" class="manage-column column-message" id="message" scope="col"><span>Message:</span><span class="sorting-indicator"></span></th>
                                <th style="" class="manage-column column-send" id="send" scope="col"><span>Notification Email:</span><span class="sorting-indicator"></span></th>
                            </thead>
                            <tfoot>
                            <tr>
                                <th style="" class="manage-column column-cb check-column" id="cb" scope="col"><input type="checkbox"></th>
                                <th style="" class="manage-column column-fname" id="fname" scope="col">Name:<span class="sorting-indicator"></span></th>
                                <th style="" class="manage-column column-email" id="email" scope="col"><span>Email Address:</span><span class="sorting-indicator"></span></th>
                                <th style="" class="manage-column column-send" id="send" scope="col"><span>Phone:</span><span class="sorting-indicator"></span></th>
                                <th style="" class="manage-column column-message" id="message" scope="col"><span>Message:</span><span class="sorting-indicator"></span></th>
                                <th style="" class="manage-column column-send" id="send" scope="col"><span>Notification Email:</span><span class="sorting-indicator"></span></th>
                            </tfoot>
                            <tbody id="the-list">
                            <?php
                           	 	global $wpdb;
								$results = $wpdb->get_results( "SELECT * FROM ".$wpdb->prefix."akto_contact_submissions");
								if (count($results)<1) echo '<tr class="no-items"><td colspan="3" class="colspanchange">No Submissions Found.</td></tr>';
								else {
									foreach($results as $row) {
                                        if(esc_js(esc_html($row->send))==1){
                                            $send= 'Sent';
                                        }else{
                                            $send= 'Not Sent';             
                                        }
										echo '<tr>
													<th class="check-column" style="padding:5px 0 2px 0"><input type="checkbox" name="rem[]" value="'.esc_js(esc_html($row->id)).'"></th>
													<td>'.esc_js(esc_html($row->fname)).'</td>
													<td>'.esc_js(esc_html($row->email)).'</td>
                                                    <td>'.esc_js(esc_html($row->phone_num)).'</td>
  													<td>'.esc_js(esc_html($row->message)).'</td>
  													<td>'.$send.'</td>
											  </tr>';
									}
								}
							?>
                            </tbody>
                        </table>
                        <br class="clear">
						<input class="button" name="submit" type="submit" value="Remove Selected" /> 
                        <input class="button" name="submit" type="submit" value="Clear all submissions" /> 
				</form>
				<br class="clear">
			</div>
            </div>
            <br class="clear">
	</div>
</div>
