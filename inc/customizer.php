<?php

/*

@package starter

	========================
		ADMIN CUSTOMIZER PAGE
	========================
*/
/**
 * Adds postMessage support for site title and description for the Customizer.
 *
 *
 * @param WP_Customize_Manager $wp_customize The Customizer object.
 */
function starter_customize_register( $wp_customize ) {
	$wp_customize->add_section( 'starter_social_section' , array(
		'title'       => __( 'Social', 'starter' ),
		'priority'    => 80,
		'description' => 'Social settings (Add link to enable the icon)',
	) );
	$wp_customize->add_setting('starter_facebook_link', array(
		'default'        => '',
		'capability'     => 'edit_theme_options',
		'type'           => 'option',
	));
	$wp_customize->add_control('starter_facebook_link', array(
		'label'      => __('Facebook link', 'starter'),
		'section'    => 'starter_social_section',
		'settings'   => 'starter_facebook_link',
	));
	$wp_customize->add_setting('starter_twitter_link', array(
		'default'        => '',
		'capability'     => 'edit_theme_options',
		'type'           => 'option',
	));
	$wp_customize->add_control('starter_twitter_link', array(
		'label'      => __('Twitter link', 'starter'),
		'section'    => 'starter_social_section',
		'settings'   => 'starter_twitter_link',
	));
	$wp_customize->add_setting('starter_instagram_link', array(
		'default'        => '',
		'capability'     => 'edit_theme_options',
		'type'           => 'option',
	));
	$wp_customize->add_control('starter_instagram_link', array(
		'label'      => __('Instagram link', 'starter'),
		'section'    => 'starter_social_section',
		'settings'   => 'starter_instagram_link',
	));
	$wp_customize->add_setting('starter_google_link', array(
		'default'        => '',
		'capability'     => 'edit_theme_options',
		'type'           => 'option',
	));
	$wp_customize->add_control('starter_google_link', array(
		'label'      => __('Google link', 'starter'),
		'section'    => 'starter_social_section',
		'settings'   => 'starter_google_link',
	));
	$wp_customize->add_setting('starter_youtube_link', array(
		'default'        => '',
		'capability'     => 'edit_theme_options',
		'type'           => 'option',
	));
	$wp_customize->add_control('starter_youtube_link', array(
		'label'      => __('Youtube link', 'starter'),
		'section'    => 'starter_social_section',
		'settings'   => 'starter_youtube_link',
	));	
	// Add  Footer Settings
	$wp_customize->add_section( 'starter_footer_section' , array(
		'title'       => __( 'Footer', 'starter' ),
		'priority'    => 90,
		'description' => 'Footer settings',
	) );
    $wp_customize->add_setting('starter_copyright_text', array(
        'default'        => 'Copyright Text',
        'capability'     => 'edit_theme_options',
        'type'           => 'option',
    ));
    $wp_customize->add_control('starter_copyright_text', array(
        'label'      => __('Copyright', 'starter'),
        'section'    => 'starter_footer_section',
        'settings'   => 'starter_copyright_text',
    ));
}
add_action( 'customize_register', 'starter_customize_register');

/**
 * Render the site title for the selective refresh partial.
 *
 *
 * @see starter_customize_register()
 *
 * @return void
 */
function starter_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 *
 * @see starter_customize_register()
 *
 * @return void
 */
function starter_customize_partial_blogdescription() {
	bloginfo( 'description' );
}
