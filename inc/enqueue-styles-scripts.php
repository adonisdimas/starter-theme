<?php

/*
@package starter

	========================
		FRONT-END ENQUEUE FUNCTIONS
	========================
*/
function starter_load_styles_scripts(){
	wp_enqueue_style( 'starter', get_template_directory_uri() . '/assets/build/css/main.min.css', array());
	if( stripos($_SERVER['HTTP_USER_AGENT'], 'Google Page Speed Insights') === false ) {
		wp_enqueue_style( 'Montserrat', 'https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i;subset=greek' );
		wp_enqueue_style( 'Open Sans', 'https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=greek' );
	}
	// Load scripts!
	wp_enqueue_script('jquery');
	wp_enqueue_script( 'jquery-ui', get_template_directory_uri() . '/assets/dev/third-party/jquery-ui/jquery-ui.min.js', array('jquery'));
	wp_enqueue_script( 'uikit', get_template_directory_uri() . '/assets/dev/third-party/uikit/dist/js/uikit.min.js', array('jquery'));
	wp_enqueue_script( 'uikit-icons', get_template_directory_uri() . '/assets/dev/third-party/uikit/dist/js/uikit-icons.min.js', array('jquery'));
	// Load scripts!
	if(get_locale() == 'el'){
		wp_enqueue_script( 'validate', get_template_directory_uri() . '/assets/dev/third-party/validate/jquery.validate-el.min.js', array('jquery'));
	}else{
		wp_enqueue_script( 'validate', get_template_directory_uri() . '/assets/dev/third-party/validate/jquery.validate.min.js', array('jquery'));
	}
	wp_enqueue_script( 'starter', get_template_directory_uri() . '/assets/build/js/main.js', array('jquery'),false,true);
	//Pass options data array to main.js
 	$data = array(
 		'base_url' => get_site_url(),
 		'locale' => get_locale(),
 		'base_assets_url' => get_template_directory_uri()
    );
	wp_localize_script('starter', 'data', $data);
}
add_action( 'wp_enqueue_scripts', 'starter_load_styles_scripts' );