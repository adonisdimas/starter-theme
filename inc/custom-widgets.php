<?php
// Social Widget
class starter_social_widget extends WP_Widget {
	function __construct() {
		$widget_ops = array(
			'classname' => 'starter-social-widget',
			'description' => 'starter Social Widget',
			'before_title'  => '<h3>',
			'after_title'   => '</h3>'
		);
		parent::__construct( 'starter_social_widget', 'starter Social', $widget_ops );
	}
	// Creating widget front-end
	public function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', $instance['title'] );
		$text = apply_filters( 'widget_text', $instance['text'] );
		// before and after widget arguments are defined by themes
		echo $args['before_widget'];
		if ( ! empty( $title ) )
			echo $args['before_title'] . $title . $args['after_title'];
		if ( ! empty( $text ) )
			echo '<p>' . $text . '</p>';
		include( locate_template( 'template-parts/social.php', false, false ) );
		// This is where you run the code and display the output
		echo __( $text, 'starter_widget_domain' );
		echo $args['after_widget'];
	}
	// Widget Backend
	public function form( $instance ) {
	$title = (isset( $instance[ 'title' ] ) ? $instance[ 'title' ] : __( '', 'starter_widget_domain' ));
	$text = (isset( $instance[ 'text' ] ) ? $instance[ 'text' ] : __( '', 'starter_widget_domain' ));
	?>
	<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
	</p>
	<p>
		<label for="<?php echo $this->get_field_id( 'text' ); ?>"><?php _e( 'Text:' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'text' ); ?>" name="<?php echo $this->get_field_name( 'text' ); ?>" type="text" value="<?php echo esc_attr( $text ); ?>" />
	</p>
	<?php
	}
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['text'] = ( ! empty( $new_instance['text'] ) ) ? strip_tags( $new_instance['text'] ) : '';
		return $instance;
	}
}
// Newsletter starter widget
class starter_newsletter_widget extends WP_Widget {
	function __construct() {
		$widget_ops = array(
			'classname' => 'starter-newsletter-widget',
			'description' => 'starter Newsletter Widget'
		);
		parent::__construct( 'starter_newsletter_widget', 'starter Newsletter', $widget_ops );
	}
	// Creating widget front-end
	public function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', $instance['title'] );
		$text = apply_filters( 'widget_text', $instance['text'] );
		//reset newsletter_submitted flag
		$input_placeholder = get_field('input_placeholder','option');
		$newsletter_messages = get_field('newsletter_messages','option');
		echo $args['before_widget'];
		// load the newsletter form template
		include( locate_template( 'inc/templates/newsletter-form.php', false, false ) );
		//update_option( 'newsletter_submitted', '-1' );
		echo $args['after_widget'];
	}
	// Widget Backend
	public function form( $instance ) {
	$title = (isset( $instance[ 'title' ] ) ? $instance[ 'title' ] : __( '', 'starter_widget_domain' ));
	$text = (isset( $instance[ 'text' ] ) ? $instance[ 'text' ] : __( '', 'starter_widget_domain' ));
	?>
	<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
	</p>
	<p>
		<label for="<?php echo $this->get_field_id( 'text' ); ?>"><?php _e( 'Text:' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'text' ); ?>" name="<?php echo $this->get_field_name( 'text' ); ?>" type="text" value="<?php echo esc_attr( $text ); ?>" />
	</p>
	<?php
	}
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['text'] = ( ! empty( $new_instance['text'] ) ) ? strip_tags( $new_instance['text'] ) : '';
		return $instance;
	}
}
// Register and load the widgets
function starter_load_widgets() {
	register_widget( 'starter_social_widget' );
	register_widget( 'starter_newsletter_widget' );
}
add_action( 'widgets_init', 'starter_load_widgets' );