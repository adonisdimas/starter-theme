<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage starter
 */
//LOAD FIELDS
$image = wp_get_attachment_image_url( get_post_thumbnail_id(get_the_ID()),'cover');
get_header(); 
	global $post;
	$children = get_pages( array( 'child_of' => $post->ID ));
	if ( is_page() && count( $children ) > 0 ){
		include( locate_template( 'template-parts/post/general-list.php', false, false ) );
	}
	else if ( is_page() && $post->post_parent ){
		include( locate_template( 'template-parts/post/general.php', false, false ) );
	}
	else{
		include( locate_template( 'template-parts/post/general.php', false, false ) );
	}
get_footer(); ?>

