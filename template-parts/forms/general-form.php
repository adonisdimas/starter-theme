<form method="post" id="formg">
    <div class="uk-margin-remove uk-padding-remove" uk-grid>
        <div class="uk-width-auto uk-margin-remove uk-padding-remove">
            <h2><?=$general_form['title'];?></h2>
        </div>
        <div class="uk-width-expand@m uk-margin-remove uk-padding-remove">
            <div class="uk-child-width-1-2@m" uk-grid>
                <div>
                    <label><?=$general_form['name'];?> *</label>
                    <input class="uk-input" type="text" name="general[firstname]" required="">
                </div>
                <div>
                    <label><?=$general_form['email'];?> *</label>
                    <input class="uk-input" type="email" name="general[email]" required="">
                </div> 
                <div>
                    <label><?=$general_form['phone'];?> *</label>
                    <input class="uk-input" type="text" name="general[phone]" required="">
                </div> 
                <div>
                    <label><?=$general_form['interested_in'];?></label>
                    <div class="select-wrapper" uk-form-custom="target: > * > span:first-child">
                        <select name="general[interested_in]">
                            <option value=""><?=$form_default_select;?></option>
                            <?php foreach ($general_form_select_options as $key => $option): ?>
                                <option value="<?=$option;?>" <?php if($key === $general_form_selected_option){echo 'selected';} ?>><?=$option;?></option>
                            <?php endforeach;?>
                        </select>
                        <button class="uk-button uk-button-default" type="button" tabindex="-1">
                            <span></span>
                            <span uk-icon="icon: chevron-down"></span>
                        </button>
                    </div>
                </div>                   
            </div>
            <div uk-grid>
                <?php include( locate_template( 'template-parts/forms/gdpr-form.php', false, false ) ); ?>
            </div> 
            <div class="uk-width-1-1 uk-child-width-1-3@m" uk-grid>
                <div class="captcha">
                    <?php echo apply_filters( 'gglcptch_display_recaptcha', '', 'starter_general_form' ); ?>
                        <input class="uk-input" type="hidden" name="captcha[captcha]">
                </div>
                <div class="submit-button" style ="display: flex;
            justify-content: flex-start;
            align-items: center;">
                    <a class="submit uk-link-heading">
                        <?=$form_submit;?>
                        <span class="uk-icon" uk-icon="icon: arrow-newsletter; ratio: 1"></span>
                    </a>
                </div>
                <?php if(isset($_GET['sent'])): ?>
                    <?php if($_GET['sent']==1): ?>
                        <div class="uk-alert-success" uk-alert>
                            <a class="uk-alert-close" uk-close></a>
                            <p><?=$general_form_messages['success_message'];?></p>
                        </div>              
                    <?php else: ?>
                        <div class="uk-alert-warning" uk-alert>
                            <a class="uk-alert-close" uk-close></a>
                            <p><?=$general_form_messages['failure_message'];?></p>
                        </div>
                    <?php endif; ?>
                <?php endif; ?>
            </div>     
        </div>
    </div>
    <input type="hidden" name="general[page_id]" value="<?php echo get_the_ID(); ?>">
</form>