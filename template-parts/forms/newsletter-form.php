<form class="newsletter" name="newsletter-form" method="post" id="newsletter-form" >
	<?php if ( $title): ?>
	<h3><?php echo $title; ?></h3>
	<?php endif; ?>
	<?php if ( $text): ?>
		<p><?php echo $text; ?></p>
	<?php endif; ?>
	<span></span>
	<input type="hidden" name="newsletter[name]" class="form-control" value="" placeholder="">
	<input type="email" required name="newsletter[email]" class="form-control" placeholder="<?php echo $input_placeholder; ?>" required/>
	<div class="submit-input" id="subscribe" uk-icon="icon: arrow-newsletter; ratio: 1"></div>
	<div id="subscribe_message" uk-grid style="    width: 100%;
    margin: 15px auto;">
		<?php if(isset($_GET['newsletter_submitted']))
			if($_GET['newsletter_submitted']==1){
				echo '<div class="uk-alert-success" uk-alert>
					  <a class="uk-alert-close" uk-close></a><p>'.$newsletter_messages['success_message'].'</p>
						</div>';
			}else if($_GET['newsletter_submitted']==0){
				echo '<div class="uk-alert-warning" uk-alert>
						    <a class="uk-alert-close" uk-close></a><p>'.$newsletter_messages['failure_message'].'</p>
						</div>';
			}else if($_GET['newsletter_submitted']==2){
				echo '<div class="uk-alert-primary" uk-alert>
						    <a class="uk-alert-close" uk-close></a><p>'.$newsletter_messages['already_exists'].'</p>
						</div>';
			}
		?>
	</div>
	<input type="hidden" name="newsletter[page_id]" value="<?php echo get_the_ID(); ?>">
	<input type="hidden" name="newsletter[honeycomb]" value="">
</form>