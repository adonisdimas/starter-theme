<form method="post" id="contact-form">
    <div class="uk-margin-remove uk-padding-remove" uk-grid>
        <div class="uk-width-auto uk-margin-remove uk-padding-remove">
            <h2><?=$contact_form['title'];?></h2>
        </div>
        <div class="uk-width-expand@m uk-margin-remove uk-padding-remove">
            <div class="uk-child-width-1-3@m" uk-grid>
                <div>
                    <label><?=$contact_form['name'];?> *</label>
                    <input class="uk-input" type="text" name="contact[firstname]" required="">
                </div>
                <div>
                    <label><?=$contact_form['email'];?> *</label>
                    <input class="uk-input" type="email" name="contact[email]" required="">
                </div>
                <div>
                    <label><?=$contact_form['phone'];?> *</label>
                    <input class="uk-input" type="text" name="contact[phone]" required="">
                </div>                     
            </div>
            <div uk-grid>
                <div class="uk-width-1-1">
                    <label><?=$contact_form['message'];?> *</label>
                    <textarea class="uk-textarea" rows="5" style="resize: none;" name="contact[message]" required=""></textarea>
                </div>   
            </div>
            <div uk-grid>
                <?php include( locate_template( 'template-parts/forms/gdpr-form.php', false, false ) ); ?>
            </div>
            <div class="uk-width-1-1 uk-child-width-1-3@m" uk-grid>
                <div class="captcha">
                    <?php echo apply_filters( 'gglcptch_display_recaptcha', '', 'starter_contact_form' ); ?>
                    <input class="uk-input" type="hidden" name="contact[captcha]">
                </div>
                <div class="submit-button" style ="display: flex;
            justify-content: flex-start;
            align-items: center;">
                    <a class="submit uk-link-heading">
                        <?=$form_submit;?>
                        <span class="uk-icon" uk-icon="icon: arrow-newsletter; ratio: 1"></span>
                    </a>
                </div>
                <?php if(isset($_GET['sent'])): ?>
                    <?php if($_GET['sent']==1): ?>
                        <div class="uk-alert-success" uk-alert>
                            <a class="uk-alert-close" uk-close></a>
                            <p><?=$contact_form_messages['success_message'];?></p>
                        </div>              
                    <?php else: ?>
                        <div class="uk-alert-warning" uk-alert>
                            <a class="uk-alert-close" uk-close></a>
                            <p><?=$contact_form_messages['failure_message'];?></p>
                        </div>
                    <?php endif; ?>
                <?php endif; ?>
            </div>    
        </div>
    </div>
    <input type="hidden" name="contact[page_id]" value="<?php echo get_the_ID(); ?>">
</form>