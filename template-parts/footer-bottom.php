<div class="bottom uk-container uk-container-large uk-text-small">
	<div class="uk-grid-match@s uk-padding-small" uk-grid>
		<div class="uk-width-1-2@m uk-text-left@m uk-text-center uk-padding-remove">
		    <p>	&copy; <?php echo date("Y"); ?> <?php echo get_option( 'starter_copyright_text'); ?></p>
		</div>	
		<div class="uk-width-1-2@m uk-text-right@m uk-text-center uk-padding-remove">
		    <p>Powered by </p>
		</div>        
	</div>
</div>