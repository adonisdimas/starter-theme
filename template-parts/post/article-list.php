<main id="article-list" class="background-grid">
	<section class="front-container">
		<?php get_template_part('template-parts/breadcrumb'); ?>	
		<div class="uk-margin-remove uk-padding-remove" uk-grid>
			<div class="uk-width-auto">
				<h1><?=apply_filters( 'the_title', get_the_title( get_option( 'page_for_posts' ) ) );?></h1>
				<div class="text-wrapper">
					<?=apply_filters( 'the_content', get_post_field( 'post_content', get_option( 'page_for_posts' ) ) );?>
				</div>
			</div>
			<div class="uk-width-expand uk-margin-remove uk-padding-remove">
				<div class=" grid uk-margin-remove uk-padding-remove uk-child-width-1-2@m uk-child-width-1-3@l" uk-grid> 
				<?php
				$args = array(
				    'post_type'      => 'post',
				    'posts_per_page' => -1,
		            'orderby'   => 'menu_order',
		            'order'     => 'ASC'
				);
				$query = new WP_Query( $args );
				$posts = $query->posts;
				$key = 1;
				foreach ($posts as $post) { 
						if($key>3){
							$key = 1;
						}
						$list_title = $post->post_title;
						$list_text = $post->post_content;
						$the_date = get_the_date('d/m/Y', $post->ID );
						//Use list image if available
						if(get_field('list_image', $post->ID)){
							$list_image_id = get_field('list_image', $post->ID);
						}else{
							$list_image_id = get_post_thumbnail_id($post->ID);
						}
						if($key ==2){
							$list_image = wp_get_attachment_image_url( $list_image_id,'starter-general-article2');
						}else{
							$list_image = wp_get_attachment_image_url( $list_image_id,'starter-general-article1');
						}
						$timing = ($key)*250;
						?>	
						<div class="grid-item uk-margin-remove@m uk-padding-remove" uk-scrollspy="cls: uk-animation-slide-bottom; repeat: false; delay: <?=$timing;?>">
							<div class="bg-wrapper">
								<a href="<?php echo get_permalink($post->ID);?>">
									<div class="title-wrapper">
										<span><?=$the_date?></span>
										<div class="uk-card-title">
											<span><?=$post->post_title;?></span>
										</div>
									</div>
									<div class="bg-image <?php if($key==2){echo 'small';}?>" style="background-image: url(<?=$list_image?>);background-position:center;background-size:cover; image-rendering: -webkit-optimize-contrast;">
									</div>
								</a>
							</div>
							<div class="text-wrapper">
								<p><?php echo wp_trim_words($list_text, $num_words = 30, $more = ' ... ' ); ?></p>
							</div>
						</div>
				<?php 
					$key=$key+1;} 
				?>
				</div>
			</div>
		</div>
	</section>
	<?php 
		$shortcuts = get_field('shortcuts','option');
		if(isset($shortcuts['box_1_youtube_id']) && isset($shortcuts['box_2_youtube_id'])){
			global $youtube_id;
			$youtube_id = $shortcuts['box_1_youtube_id'];
			include( locate_template( 'template-parts/youtube-modal-gallery.php', false, false ) );
			$youtube_id = $shortcuts['box_2_youtube_id'];
			include( locate_template( 'template-parts/youtube-modal-gallery.php', false, false ) );
		}
	?>
	<section id="shortcuts">
		<?php include( locate_template( 'template-parts/shortcuts.php', false, false ) ); ?>
	</section>
</main>