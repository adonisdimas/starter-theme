
<?php
	$general_labels = get_field('general_labels','option');
	$image = wp_get_attachment_image_url( get_post_thumbnail_id(get_the_ID()),'starter-taxonomy-course-cover');
	$content = get_field('text');
	$when_text = get_field('when_text');
	$when_text2 = get_field('when_text_2');
	//Add the date seperator if second date exists
	if(isset($when_text2)){
		$when_text2 = ' - '.$when_text2;
	}
	$where_text = get_field('where_text');
	$content = get_field('text');
	$content_image =  wp_get_attachment_image_url(get_field('image'),'starter-course-content');
	$gallery = get_field('gallery');
	$args = array(
	    'post_type'      => 'post',
	    'post__not_in' => array( get_the_ID() ), //do not show the current post in the related 
	    'posts_per_page' => 9,
	);
	$query = new WP_Query( $args );
	$related = $query->posts;
?>
<main id="article" class="background-grid">
	<section class="front-container">
		<div class="uk-margin-remove uk-padding-remove" uk-grid>
			<div class="uk-width-auto uk-margin-remove uk-padding-remove">
			</div>
			<div class="uk-width-expand uk-margin-remove uk-padding-remove" style="position:relative;">
				<div class="bg-wrapper">
					<div class="uk-animation-slide-right" style="background-image: url(<?=$image;?>);background-position:center;background-size:cover;image-rendering: -webkit-optimize-contrast; ">
					</div>
				</div>
			</div>
			<a class="view uk-link-heading mobile-back" href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>">
				<span class="uk-icon" uk-icon="icon: arrow1-left; ratio: 1"></span>
				<?=$general_labels['back_to_title'];?>
			</a>
			<h1>
				<span><?php the_title();?></span>	
			</h1>
			<div class="infos">
				<?php if(isset($when_text) && !empty($when_text)): ?> 
					<div class="info-wrapper">
						<span><?=$general_labels['when_title'];?></span>
						<p><?=$when_text;?><?=$when_text2;?></p>
					</div>
				<?php endif; ?>
				<?php if(isset($where_text) && !empty($where_text)): ?> 
					<div class="info-wrapper last">
						<span><?=$general_labels['where_title'];?></span>
						<p><?=$where_text;?></p>
					</div>
				<?php endif; ?>
			</div>
		</div>	
	</section>
	<?php if(isset($gallery) && $gallery ): ?> 
		<div class="thumb-gallery">
			<p><?=$general_labels['gallery_title']?></p>
			<div class="uk-child-width-1-3 uk-margin-remove uk-padding-remove" uk-grid uk-lightbox="animation: slide">
				<?php foreach ($gallery as $key => $gallery_item): 
					$thumb = wp_get_attachment_image_url($gallery_item['id'],'starter-course-gallery-thumb');
					?>
					<div class="uk-margin-remove uk-padding-remove">
						<a class="uk-inline" href="<?=$gallery_item['url'];?>" data-caption="<?=$gallery_item['caption'];?>">
							<img src="<?=$thumb;?>" alt="">
						</a>
					</div>
				<?php endforeach;?>
			</div>					
		</div>
	<?php endif; ?>	
	<section class="content">
		<div class="uk-margin-remove uk-padding-remove" uk-grid>
			<div class="uk-width-auto uk-margin-remove uk-padding-remove">
			</div>
			<div class="uk-width-expand uk-margin-remove uk-padding-remove">
				<div class="text-wrapper">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</section>
	<?php if(isset($content) && !empty($content)): ?> 
		<section class="three-column-content">
			<div class="uk-margin-remove uk-padding-remove" uk-grid uk-scrollspy="cls: uk-animation-slide-right; repeat: false; delay: 500;">
				<div class="background-grid uk-width-auto uk-margin-remove uk-padding-remove" uk-scrollspy="cls: uk-animation-slide-bottom; repeat: false; delay: 1000;">
					<a class="view uk-link-heading" href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>">
						<span class="uk-icon" uk-icon="icon: arrow1-left; ratio: 1"></span>
						<?=$general_labels['back_to_title'];?>
					</a>
					<?php if(isset($content_image) && !empty($content_image)): ?> 
						<div class="image-wrapper">
							<div class="bg-wrapper">
								<div class="uk-animation-slide-left" style="background-image: url(<?=$content_image;?>);background-position:center;background-size:cover; ">
								</div>
							</div>
						</div>
					<?php endif; ?>
				</div>
				<div class="uk-width-expand uk-margin-remove uk-padding-remove" uk-scrollspy="cls: uk-animation-slide-bottom; repeat: false; delay: 1200;">
					<div class="text-wrapper">
						<?=$content;?>
					</div>
				</div>
			</div>
		</section>
	<?php endif; ?>
	<?php if(isset($related) && !empty($related)): ?> 
		<section class="related">
			<div class="uk-margin-remove uk-padding-remove" uk-grid>
				<div class="uk-width-auto@m uk-margin-remove@m uk-padding-remove@m">
					<div class="title-wrapper">
						<span><?=$general_labels['more_news_title'];?></span>
					</div>
				</div>
				<div class="uk-width-expand@m uk-margin-remove@m uk-padding-remove@m">
					<div class="uk-position-relative uk-visible-toggle uk-light" uk-slider>
						<?php include( locate_template( 'template-parts/news-carousel.php', false, false ) ); ?>
					</div>
					<a class="view uk-link-heading" href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>">
							<?=$general_labels['view_all_news_title'];?>
						<span class="uk-icon" uk-icon="icon: arrow1-right; ratio: 1"></span>
					</a>	
				</div>
			</div>
		</section>
	<?php endif; ?>
</main>