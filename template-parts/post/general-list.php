<?php
//GET the children of this page
$args = array(
    'post_type'      => 'page',
    'posts_per_page' => -1,
    'post_parent'    => $post->ID,
    'orderby'   => 'menu_order',
    'order'     => 'ASC'
);
$query = new WP_Query( $args );
$posts = $query->posts;
?>
<main id="general-list" class="background-grid">
	<section class="front-container">
		<div class="uk-margin-remove uk-padding-remove" uk-grid>
			<div class="uk-width-auto uk-margin-remove uk-padding-remove">
			</div>
			<h1 class="desktop-title"><?php the_title();?></h1>
			<div class="uk-width-expand uk-margin-remove uk-padding-remove">
				<div class="bg-color uk-animation-slide-right"></div>
				<div class="bg-wrapper">
					<div class="uk-animation-slide-right" style="background-image: url(<?=$image?>);background-position:center;background-size:cover;image-rendering: -webkit-optimize-contrast; ">
					</div>
				</div>
				<h1 class="mobile-title"><?php the_title();?></h1>
				<div class="text-wrapper">
					<p><?php the_content();?></p>
				</div>
			</div>
		</div>
		<?php get_template_part('template-parts/breadcrumb'); ?>	
	</section>
	<section class="listing">
		<?php foreach ($posts as $key => $post) { 
			$list_title = $post->post_title;
			$list_text = $post->post_content;
			$list_image = wp_get_attachment_image_url( get_post_thumbnail_id($post->ID),'starter-general-list');
			?>	
			<div class="uk-margin-remove uk-padding-remove uk-child-width-1-4@m" uk-grid uk-scrollspy="cls: uk-animation-slide-bottom; repeat: false; delay: 500;"> 
			    <div class="uk-margin-remove uk-padding-remove uk-flex">
			        <div class="uk-flex">
			            <a class="uk-link-heading" href="<?php echo get_permalink($post->ID);?>">
			            	<h3 class="uk-card-title"><?=$list_title;?></h3>
			            </a>
			        </div>
			    </div>
			    <div class="uk-margin-remove uk-padding-remove uk-width-expand@m uk-flex">
			        <div class="uk-flex">
			            <p><?php echo wp_trim_words($list_text, $num_words = 40, $more = ' ... ' ); ?></p>
						<a class="view" href="<?php echo get_permalink($post->ID);?>"><?=$general_labels['read_more_label']?> <span class="uk-icon" uk-icon="icon: arrow-newsletter; ratio: 1"></span></a>	            
			        </div>
			    </div>
			    <div class="uk-margin-remove uk-padding-remove uk-flex">
			        <div class="uk-flex">
						<a href="<?php echo get_permalink($post->ID);?>">
							<div class="bg-image" style="background-image: url(<?=$list_image?>);background-position:center;background-size:cover;image-rendering: -webkit-optimize-contrast; ">
							</div>
						</a>
			        </div>
			    </div>
			    <div class="uk-margin-remove uk-padding-remove uk-flex uk-hidden@m">
			        <div class="uk-flex">
			            <a class="uk-link-heading" href="<?php echo get_permalink($post->ID);?>">
			            	<h3 class="uk-card-title"><?=$list_title;?></h3>
			            </a>
			        </div>
			    </div>	
			</div>
		<?php } ?>
	</section>
</main>