<main id="general" class="background-grid">
	<section class="front-container">
		<div class="uk-margin-remove uk-padding-remove" uk-grid>
			<div class="uk-width-auto uk-margin-remove uk-padding-remove"></div>
			<h1 class="desktop-title"><?php the_title();?></h1>
			<div class="uk-width-expand uk-margin-remove uk-padding-remove">
				<div class="bg-color uk-background-secondary uk-animation-slide-right"></div>
				<div class="bg-wrapper">
					<div class="uk-animation-slide-right" style="background-image: url(<?=$image;?>);background-position:center;background-size:cover;image-rendering: -webkit-optimize-contrast; ">
					</div>
				</div>
			</div>
		</div>	
		<?php get_template_part('template-parts/breadcrumb'); ?>	
		<?php get_template_part('template-parts/starter-animated'); ?>
		<h1 class="mobile-title"><?php the_title();?></h1>
	</section>
	<?php if(isset($gallery) && $gallery ): ?> 
		<div class="thumb-gallery">
			<p><?=$general_labels['gallery_title']?></p>
			<div class="uk-child-width-1-3 uk-margin-remove uk-padding-remove" uk-grid uk-lightbox="animation: slide">
				<?php foreach ($gallery as $key => $gallery_item): 
					$thumb = wp_get_attachment_image_url($gallery_item['id'],'starter-course-gallery-thumb');
					?>
					<div class="uk-margin-remove uk-padding-remove">
						<a class="uk-inline" href="<?=$gallery_item['url'];?>" data-caption="<?=$gallery_item['caption'];?>">
							<img src="<?=$thumb;?>" alt="">
						</a>
					</div>
				<?php endforeach;?>
			</div>					
		</div>
	<?php endif; ?>	
	<section class="content">
		<div class="uk-margin-remove uk-padding-remove" uk-grid>
			<div class="uk-width-auto uk-margin-remove uk-padding-remove">
			</div>
			<div class="uk-width-expand uk-margin-remove uk-padding-remove">
				<?php if(!empty(get_the_content())): ?> 
					<div class="text-wrapper">
						<?php the_content(); ?>
					</div>
				<?php endif; ?>	
				<?php if(isset($collapsible_tabs) && !empty($collapsible_tabs['faqs']) ): ?> 
					<div class="faqs">
						<ul uk-accordion="collapsible: true;multiple: true;">
							<?php foreach ($collapsible_tabs['faqs'] as $key => $value): 
										$title = $value['title'];
										$content = $value['content'];
								?>
								<li>
									<a class="uk-accordion-title">
										<?=$title;?>
									</a>
									<div class="uk-accordion-content">
										<?=$content;?>
									</div>
								</li>
							<?php endforeach;?>
						</ul>				
					</div>
				<?php endif; ?>	
			</div>
		</div>
	</section>
	<?php if(isset($content1['text']) && !empty($content1['text'])): ?> 
		<section class="three-column-content">
			<div class="uk-margin-remove uk-padding-remove" uk-grid uk-scrollspy="cls: uk-animation-slide-right; repeat: false; delay: 500;">
				<div class="background-grid uk-width-auto uk-margin-remove uk-padding-remove" uk-scrollspy="cls: uk-animation-slide-bottom; repeat: false; delay: 1000;">
					<div class="image-wrapper">
						<div class="bg-wrapper">
							<div class="uk-animation-slide-left" style="background-image: url(<?=$image2;?>);background-position:center;background-size:cover;image-rendering: -webkit-optimize-contrast; ">
							</div>
						</div>
						<span class="caption"><?=$content1['image']['caption'];?></span>
					</div>
				</div>
				<div class="uk-width-expand uk-margin-remove uk-padding-remove" uk-scrollspy="cls: uk-animation-slide-bottom; repeat: false; delay: 1200;">
					<div class="text-wrapper">
						<?=$content1['text'];?>
					</div>
				</div>
			</div>
		</section>
	<?php endif; ?>
	<?php if(isset($image3) && !empty($image3)): ?> 
		<section class="container-with-image">
			<div class="uk-margin-remove uk-padding-remove" uk-grid>
				<div class="uk-width-expand uk-margin-remove uk-padding-remove">
					<div class="image-wrapper" uk-scrollspy="cls: uk-animation-slide-right; repeat: false; delay: 650;">
						<div class="bg-wrapper">
							<div style="background-image: url(<?=$image3;?>);background-position:center;background-size:cover; ">
							</div>
						</div>
						<span class="caption"><?=$content2['image']['caption'];?></span>
					</div>
				</div>
			</div>
		</section>
	<?php endif; ?>
	<?php if(isset($content2['text']) && !empty($content2['text'])): ?> 
		<section class="content">
			<div class="uk-margin-remove uk-padding-remove" uk-grid>
				<div class="uk-width-auto uk-margin-remove uk-padding-remove">
				</div>
				<div class="uk-width-expand uk-margin-remove uk-padding-remove">
					<div class="text-wrapper">
						<?=$content2['text'];?>
					</div>
				</div>
			</div>
		</section>
	<?php endif; ?>
	<?php if(isset($content3['related_posts']) && !empty($content3['related_posts'])): ?>
		<section class="related">
			<div class="uk-margin-remove uk-padding-remove" uk-grid>
				<div class="uk-width-auto uk-margin-remove uk-padding-remove">
					<div class="image-wrapper">
						<div class="bg-color"></div>
						<div class="bg-wrapper">
							<div style="background-image: url(<?=$image4;?>);background-position:center;background-size:cover; ">
							</div>
						</div>
						<span class="caption"><?=$content3['image']['caption'];?></span>
					</div>
				</div>
				<div class="uk-width-expand uk-margin-remove uk-padding-remove">
					<div class="uk-card uk-card-body uk-margin-top">
						<div class="title-wrapper">
							<span><?=$general_labels['related_label']?></span>
						</div>						
						<ul class="uk-nav">
							<?php foreach ($content3['related_posts'] as $key => $related_post): ?>
								<li><a href="<?=get_permalink($related_post->ID)?>"><span uk-icon="icon: chevron-right; ratio: 1"></span>
								<?=$related_post->post_title;?>
								</a></li>
							<?php endforeach; ?>
						</ul>						
					</div>
				</div>
			</div>
		</section>
	<?php endif; ?>
</main>