<?php 
global $wp_query; // get the global object
$thesearch = get_search_query(); // get the string searched
// merge them with one or several meta_queries to meet your demand
$args = array_merge( array( 'post_type' => 'any' ),$wp_query->query);
$search = new WP_Query( $args);
get_header(); ?>
<main id="general" class="background-grid search">
	<section class="front-container">
		<div class="uk-margin-remove uk-padding-remove" uk-grid>
			<div class="uk-width-auto uk-margin-remove uk-padding-remove"></div>
			<h1>SITE SEARCH</h1>
			<div class="bg-wrapper">
				<div class="uk-animation-slide-right" style="background-image: url(<?=$image;?>);background-position:center;background-size:cover; ">
				</div>
			</div>
		</div>	
	</section>
	<section class="content">
		<div class="uk-margin-remove uk-padding-remove" uk-grid>
			<div class="uk-width-auto uk-margin-remove uk-padding-remove">
			</div>
			<div class="uk-width-expand uk-margin-remove uk-padding-remove">
				<div class="text-wrapper">
					<p><span>TOTAL SEARCH RESULTS:</span> <?=sizeof($search->posts);?></p>
					<p><?php 
						$i = 0;
						if( $search->have_posts()): 
						while( $search->have_posts()): $search->the_post();
						{
							echo "<a href=".get_permalink($search->posts[$i]->ID).">".$search->posts[$i]->post_title. "</a>";
							echo "<br>";
							$i++;
						}
						endwhile; 
						else:
							echo "NO RESULTS FOUND";
						endif;
					?></p>
				</div>
			</div>
		</div>
	</section>
</main>
<?php get_footer(); ?>