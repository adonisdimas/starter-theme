<?php
get_header();
$post_type = get_post_type();
if ( $post_type === 'course') {
	include( locate_template( 'template-parts/post/course.php', false, false ) );
}else{
	include( locate_template( 'template-parts/post/article.php', false, false ) );
}
get_footer(); ?>