<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage starter
 * @since 1.0
 * @version 1.2
 */
$partners = get_field('partners','option');
?>
<footer id="footer">
	<div class="top uk-background-secondary">
		<div class="uk-container uk-container-large uk-padding">
			<div class="uk-grid-match uk-padding marginleft" uk-grid>
				<div class="uk-width-expand uk-text-left uk-padding-remove">
					<div class="logo">
						<span uk-icon="logo"></span>
					</div>
				</div>
				<div class="addresses uk-width-3-5@m uk-text-left@m uk-text-center uk-padding-remove">
					<div uk-grid>
						<div class="uk-width-1-1@m uk-child-width-1-2@m" uk-grid>
							<?php dynamic_sidebar( 'footer-area' ); ?>
						</div>
					</div>
				</div>
				<div class="partners uk-width-expand@m uk-text-left@m uk-text-center uk-padding-remove">
					<a href="<?=$partners['partner_1_link'];?>" target="_new">
						<img src="<?=$partners['partner_1_image'];?>"/>
					</a>
					<a href="<?=$partners['partner_2_link'];?>" target="_new">
						<img src="<?=$partners['partner_2_image'];?>"/>
					</a>
					<a href="<?=$partners['partner_3_link'];?>" target="_new">
						<img src="<?=$partners['partner_3_image'];?>"/>
					</a>
				</div>
			</div>
		</div>
	</div>
	<?php get_template_part('template-parts/footer-bottom'); ?>
</footer>
<?php wp_footer(); ?>
</body>
</html>
