## Wordpress Starter Theme

This is a wordpress starter theme. It has been developed with the aim to give a kickstart for the development of complex wordpress powered sites 
while also trying to make the code structure easy to understand and maintain
It has a flexible frontend assets framework with the addition of UIkit (https://getuikit.com/)
The code structure of the theme strictly follows Wordpress theming as found in the documentation here: https://developer.wordpress.org/themes/getting-started/
No other template engines or coding practices are used as in other popular starter themes like sage (https://roots.io/sage/) or timber (https://www.upstatement.com/timber/)


## Key Features

1. Build in frontend assets framework using sass (https://sass-lang.com/) and gulp (https://gulpjs.com/), with ready to run automated tasks to build the assets. 
2. Inclusion of the powerfull front-end framework UIkit (https://getuikit.com/)
3. A robust and easy to understand file structure for page, post templates and wordpress function files
4. Ready to use files with example code for creating custom post types, custom widgets, custom widget areas, custom database tables and custom theme hooks
5. Build in and ready to use custom widgets (social links and newsletter form)
6. Build in admin interface for managing newsletters and contact submitions
7. Build in emailer to send notification emails for the submitted forms
8. Build in and ready to used frequently used acf fields (https://www.advancedcustomfields.com/), exported in the acf-export file that can be found in the root folder of the theme
