<?php
/**
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * Displays content for front page
 *
 * @package WordPress
 * @subpackage akto
 */
//FIrstly load all the custom fields of homepage
$home_slider = get_field('home_slider');
$art_and_design = get_field('art_and_design');
$shortcuts = get_field('shortcuts','option');
$news_and_events = get_field('news_and_events');
$levels = get_terms(['taxonomy' => 'course_level','hide_empty' => false]);
$faculties = get_terms(['taxonomy' => 'course_faculty','hide_empty' => false]);
get_header();
if(isset($shortcuts['box_1_youtube_id']) && isset($shortcuts['box_2_youtube_id'])){
	global $youtube_id;
	$youtube_id = $shortcuts['box_1_youtube_id'];
	include( locate_template( 'template-parts/youtube-modal-gallery.php', false, false ) );
	$youtube_id = $shortcuts['box_2_youtube_id'];
	include( locate_template( 'template-parts/youtube-modal-gallery.php', false, false ) );
}
?>
<main>
	<a id="scroll-down" href="#art-and-design" uk-scroll><?= $general_labels['scroll_down_label']; ?> <span uk-icon="arrow1-right" ></span></a>
	<section id="home-slider">
		<?php include( locate_template( 'template-parts/home-slider.php', false, false ) ); ?>
	</section>
	<?php //if (wp_is_mobile()): ?>
		<section id="art-and-design-mobile">
			<?php include( locate_template( 'template-parts/art-and-design-mobile.php', false, false ) ); ?>
		</section>
	<?php //else: ?>
		<section id="art-and-design">
			<?php include( locate_template( 'template-parts/art-and-design.php', false, false ) ); ?>
		</section>
	<?php //endif; ?>
	<section id="shortcuts">
		<?php include( locate_template( 'template-parts/shortcuts.php', false, false ) ); ?>
	</section>
	<section id="news-and-events">
		<?php include( locate_template( 'template-parts/news-and-events.php', false, false ) ); ?>
	</section>
	<section id="akto-life">
		<?php include( locate_template( 'template-parts/akto-life.php', false, false ) ); ?>
	</section>
</main>
<?php get_footer();